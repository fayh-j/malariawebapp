from django.db import models
from django.utils import timezone


class Hopital(models.Model):
    name = models.CharField(max_length=100, verbose_name="Nom", unique=True )
    logo = models.ImageField(upload_to='images/', null=True, blank=True, verbose_name="Logo du site")
    phone = models.IntegerField( help_text='Numéro de téléphone', verbose_name="Numéro de téléphone", unique=True)
    mail = models.EmailField(max_length=50, verbose_name="Adresse mail contact", unique=True)
    adresse = models.TextField(null=True, blank=True , default="#", verbose_name="Adresse")
   
    class Meta:
        verbose_name = "Hopital"
        #ordering = ['name']

    def __str__(self):
        return self.name

class Patient(models.Model):
    firstname = models.CharField(max_length=100, verbose_name="Prénoms")
    lastname = models.CharField( max_length=500, verbose_name="Nom")
    phone1 = models.IntegerField( help_text='Numéro de téléphone', verbose_name="Numéro de téléphone", unique=True)
    mail = models.EmailField(max_length=50, verbose_name="Adresse mail", unique=True)
    adresse = models.TextField(null=True, blank=True , default="#", verbose_name="Adresse")
    photo = models.ImageField(upload_to='images/',null=True, blank=True, verbose_name="Photo", default='images/photo.png')
   
    class Meta:
        verbose_name = "Patient"
        #ordering = ['name']

    def __str__(self):
        return self.firstname+' '+self.lastname


class Agent(models.Model):
    firstname = models.CharField(max_length=100, verbose_name="Prénoms")
    lastname = models.CharField( max_length=500, verbose_name="Nom")
    phone1 = models.IntegerField( help_text='Numéro de téléphone', verbose_name="Numéro de téléphone", unique=True)
    mail = models.EmailField(max_length=50, verbose_name="Adresse mail", unique=True)
    adresse = models.TextField(null=True, blank=True , default="#", verbose_name="Adresse")
    photo = models.ImageField(upload_to='images/',null=True, blank=True, verbose_name="Photo")
    hopital = models.OneToOneField(Hopital, null=True, on_delete = models.PROTECT)
   
    class Meta:
        verbose_name = "Agent médical"

    def __str__(self):
        return self.firstname+' '+self.lastname



class Analyse(models.Model):
    created_at = models.DateTimeField(default=timezone.now, verbose_name="Date de création")
    updated_at = models.DateTimeField(default=timezone.now, verbose_name="Date de supression")
    patient = models.ForeignKey(Patient, null=True, on_delete = models.PROTECT)
    infected_cells = models.IntegerField(verbose_name="Nombre de cellules infectées",null=True, blank=True  )
    #infected_cells = models.IntegerField(max_length=100, verbose_name="Nombre de cellules infectées" )
    class Meta:
        verbose_name = "Analyse"
        #ordering = ['name']

    def __str__(self):
    	return self.patient.firstname+ ',  '+ self.patient.lastname+ ',  '+ str(self.created_at)



class Image(models.Model):
    file = models.ImageField(upload_to='media/analyse/', verbose_name="Champs de vision")
    analyse = models.ForeignKey(Analyse, null=True,  on_delete = models.CASCADE)
    class Meta:
        verbose_name = "Champs de vision"
        #ordering = ['name']

    def __str__(self):
    	return self.file.path