from django.contrib import admin
from main.models import *
from django.utils.translation import gettext_lazy as _
from django.conf import settings
from django import template
from django.template.defaultfilters import stringfilter
 

admin.site.index_title = _('Administration')
admin.site.site_header = _('Administration')
admin.site.site_title = _('Administration')



admin.site.register(Hopital)
admin.site.register(Analyse)
admin.site.register(Patient)
admin.site.register(Image)
admin.site.register(Agent)
