# Generated by Django 2.1.7 on 2019-11-05 11:43

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0007_image_description'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='image',
            name='description',
        ),
    ]
