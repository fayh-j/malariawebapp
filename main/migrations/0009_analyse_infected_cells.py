# Generated by Django 2.1.7 on 2019-11-05 12:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0008_remove_image_description'),
    ]

    operations = [
        migrations.AddField(
            model_name='analyse',
            name='infected_cells',
            field=models.IntegerField(default=3, max_length=100, verbose_name='Nombre de cellules infectées'),
            preserve_default=False,
        ),
    ]
