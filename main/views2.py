from django.shortcuts import render
from django.http import  HttpResponse, HttpResponseRedirect, JsonResponse
from django.core import serializers
from main.models import *
from main.forms import *
from django.utils.translation import *
from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User
from django.contrib.auth.hashers import make_password
import os, sys
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
from django.views.decorators.csrf import csrf_exempt
sys.path.append('/root/malariawebapp/')
#import inference


def home(request):
	return render(request, 'index.html', locals())


def getlogin(request):
	return render(request, 'login.html', locals())


def analyse_list(request, user, patientPk):
	patient = Patient.objects.get(pk = patientPk)
	analyses = Analyse.objects.filter(patient=Patient.objects.get(pk=patientPk) )
	return render(request, 'analyses.html', locals())


def new_analyse(request, user, patientPk):
	imageForm = ImgForm()
	patient = Patient.objects.get(pk = patientPk)
	analyse = Analyse(patient = patient)
	analyse.save()
	return render(request, 'analyse.html', locals())

def login_user(request):
	patients = Patient.objects.all()
	try:
		user = User.objects.get( username= request.POST.get('username'))
		if user.check_password(request.POST.get('password')):
			return render(request, 'patients.html', locals())

	except Exception as e:
		pass
	return HttpResponseRedirect('/')
	


def logout_view(request):
    logout(request)



@csrf_exempt
def addImage(request, analysePk):
	if request.method == 'POST':
		form = ImgForm(request.POST, request.FILES)
		if form.is_valid():
			img = form.save()
			img.analyse = Analyse.objects.get(pk=analysePk)
			img.save()
			return JsonResponse({'error': False, 'message': 'success'})
			#"""
			pass
		else:
			return JsonResponse({'error': True, 'errors': form.errors})
	else:
		return HttpResponse('Seule la méthode POST est autorisée')


def make_analyse(request, analysePk):
	images = Image.objects.filter( analyse=Analyse.objects.get(pk=analysePk) )
	#return HttpResponse( images[0].file.path )	
	for image in images:
		a =os.popen('cp  '+ image.file.path.split('/www/')[1] +'   malariawebapp/media/analyse/').read() 
		#return HttpResponse('copy: '+ image.file.path.split('/www/')[1] )
	predictions = os.popen('cd malariawebapp/main && pwd &&  python3 inference.py').read()
	return JsonResponse({'predictions': predictions})

def atest(request):
	#os.system("cd malariawebapp/main")
	import subprocess
	cmd = 'cd malariawebapp/main/ && ls && python3 inference2.py'
	p = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True)
	out, err = p.communicate()
	subprocess.call(cmd, shell=True)
	return HttpResponse(out, err)
