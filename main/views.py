from django.shortcuts import render
from django.http import  HttpResponse, HttpResponseRedirect, JsonResponse
from django.core import serializers
from main.models import *
from main.forms import *
from django.utils.translation import *
from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User
from django.contrib.auth.hashers import make_password
import os, sys
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
from django.views.decorators.csrf import csrf_exempt
sys.path.append('/root/malariawebapp/')
#import inference


import numpy as np
import os
import six.moves.urllib as urllib
import sys
import tarfile
import tensorflow as tf
import zipfile

from distutils.version import StrictVersion
from collections import defaultdict
from io import StringIO
from matplotlib import pyplot as plt
from PIL import Image


def load_image_into_numpy_array(image):
  (im_width, im_height) = image.size
  return np.array(image.getdata()).reshape(
      (im_height, im_width, 3)).astype(np.uint8)


def run_inference_for_single_image(image, graph):
  with graph.as_default():
    with tf.Session() as sess:
      # Get handles to input and output tensors
      ops = tf.get_default_graph().get_operations()
      all_tensor_names = {output.name for op in ops for output in op.outputs}
      tensor_dict = {}
      for key in [
          'num_detections', 'detection_boxes', 'detection_scores',
          'detection_classes', 'detection_masks'
      ]:
        tensor_name = key + ':0'
        if tensor_name in all_tensor_names:
          tensor_dict[key] = tf.get_default_graph().get_tensor_by_name(
              tensor_name)
      if 'detection_masks' in tensor_dict:
        # The following processing is only for single image
        detection_boxes = tf.squeeze(tensor_dict['detection_boxes'], [0])
        detection_masks = tf.squeeze(tensor_dict['detection_masks'], [0])
        # Reframe is required to translate mask from box coordinates to image coordinates and fit the image size.
        real_num_detection = tf.cast(tensor_dict['num_detections'][0], tf.int32)
        detection_boxes = tf.slice(detection_boxes, [0, 0], [real_num_detection, -1])
        detection_masks = tf.slice(detection_masks, [0, 0, 0], [real_num_detection, -1, -1])
        detection_masks_reframed = utils_ops.reframe_box_masks_to_image_masks(
            detection_masks, detection_boxes, image.shape[1], image.shape[2])
        detection_masks_reframed = tf.cast(
            tf.greater(detection_masks_reframed, 0.5), tf.uint8)
        # Follow the convention by adding back the batch dimension
        tensor_dict['detection_masks'] = tf.expand_dims(
            detection_masks_reframed, 0)
      image_tensor = tf.get_default_graph().get_tensor_by_name('image_tensor:0')

      # Run inference
      output_dict = sess.run(tensor_dict,
                             feed_dict={image_tensor: image})

      # all outputs are float32 numpy arrays, so convert types as appropriate
      output_dict['num_detections'] = int(output_dict['num_detections'][0])
      output_dict['detection_classes'] = output_dict[
          'detection_classes'][0].astype(np.int64)
      output_dict['detection_boxes'] = output_dict['detection_boxes'][0]
      output_dict['detection_scores'] = output_dict['detection_scores'][0]
      if 'detection_masks' in output_dict:
        output_dict['detection_masks'] = output_dict['detection_masks'][0]
  return output_dict

def run(TEST_IMAGE_PATHS):
  import os
  os.popen("cd /var/www/malariawebapp/main/models/research/object_detection/")
  print('run 0') 
  # Size, in inches, of the output images.
  IMAGE_SIZE = (12, 8)

  print('run 1') 
  PATH_TO_FROZEN_GRAPH = '/var/www/malariawebapp/main/datalab/frozen_inference_graph.pb'
  # List of the strings that is used to add correct label for each box.
  PATH_TO_LABELS = '/var/www/malariawebapp/main/datalab/label_map_malaria.pbtxt'

  #load model to memory
  detection_graph = tf.Graph()
  with detection_graph.as_default():
    od_graph_def = tf.GraphDef()
    with tf.gfile.GFile(PATH_TO_FROZEN_GRAPH, 'rb') as fid:
      serialized_graph = fid.read()
      od_graph_def.ParseFromString(serialized_graph)
      tf.import_graph_def(od_graph_def, name='')

  print('run 2') 
  import main.label_map_util as lmu
  print('run 4') 
  import main.visualization_utils as vis_util
  print('run 5') 
  category_index = lmu.create_category_index_from_labelmap(PATH_TO_LABELS)

  import cv2
  print('run 3') 
  outPaths = []
  for image_path in TEST_IMAGE_PATHS:
    image = Image.open(image_path)
    print(image_path)
    # the array based representation of the image will be used later in order to prepare the
    # result image with boxes and labels on it.
    image_np = load_image_into_numpy_array(image)
    # Expand dimensions since the model expects images to have shape: [1, None, None, 3]
    image_np_expanded = np.expand_dims(image_np, axis=0)
    # Actual detection.
    output_dict = run_inference_for_single_image(image_np_expanded, detection_graph)
    # Visualization of the results of a detection.
    vis_util.visualize_boxes_and_labels_on_image_array(
        image_np,
        output_dict['detection_boxes'],
        output_dict['detection_classes'],
        output_dict['detection_scores'],
        category_index,
        instance_masks=output_dict.get('detection_masks'),
        use_normalized_coordinates=True,
        line_thickness=8)
    #plt.figure(figsize=IMAGE_SIZE)
    #plt.imshow(image_np)
    print(image_path)
    newPath= '/var/www/malariawebapp/media/predictions/'+ image_path.split('/analyse/')[1]
    outPaths.append(newPath)
    cv2.imwrite( newPath  , image_np)
 





def home(request):
	return render(request, 'index.html', locals())


def inference(request):
	return render(request, 'inference.html', locals())


def getlogin(request):
	return render(request, 'login.html', locals())


def analyse_list(request, user, patientPk):
	patient = Patient.objects.get(pk = patientPk)
	analyses = Analyse.objects.filter(patient=Patient.objects.get(pk=patientPk) )
	return render(request, 'analyses.html', locals())


def new_analyse(request, user, patientPk):
	imageForm = ImgForm()
	patient = Patient.objects.get(pk = patientPk)
	analyse = Analyse(patient = patient)
	analyse.save()
	return render(request, 'analyse.html', locals())

def login_user(request):
	patients = Patient.objects.all()
	try:
		user = User.objects.get( username= request.POST.get('username'))
		if user.check_password(request.POST.get('password')):
			return render(request, 'patients.html', locals())

	except Exception as e:
		pass
	return HttpResponseRedirect('/')
	


def logout_view(request):
    logout(request)



@csrf_exempt
def addImage(request, analysePk):
	if request.method == 'POST':
		form = ImgForm(request.POST, request.FILES)
		if form.is_valid():
			img = form.save()
			predictions = os.popen(cmd = 'cd /content/malariawebapp/main/ && ls && python3 inference3.py').read()
			return HttpResponse( 'success: \n '+predictions)
			pass
		else:
			return HttpResponse( 'errors'+ form.errors)
	else:
		return HttpResponse('Seule la méthode POST est autorisée')


def make_analyse(request, analysePk):
	images = Image.objects.filter( analyse=Analyse.objects.get(pk=analysePk) )
	#return HttpResponse( images[0].file.path )	
	for image in images:
		a =os.popen('cp  '+ image.file.path.split('/www/')[1] +'   malariawebapp/media/analyse/').read() 
		#return HttpResponse('copy: '+ image.file.path.split('/www/')[1] )
	predictions = os.popen('cd malariawebapp/main && pwd &&  python3 inference.py').read()
	return JsonResponse({'predictions': predictions})

def atest(request):
	#os.system("cd malariawebapp/main")
	import subprocess
	cmd = 'cd malariawebapp/main/ && ls && python3 inference2.py'
	p = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True)
	out, err = p.communicate()
	imgs= [ '/var/www/malariawebapp/media/analyse/' + img_name for img_name in os.popen('ls /var/www/malariawebapp/media/analyse/').read().split('\n')[:-1] ]
	"""print('run inference')
	print( os.popen('pwd').read() )
	print(imgs)"""
	run(imgs)
