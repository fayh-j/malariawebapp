from django.contrib import admin
from django.urls import include, path
from main import views

urlpatterns = [
    path('', views.home, name='accueil'),
    path('getlogin', views.getlogin),
    path('login', views.login_user),
    path('analyse_list/<str:user>/<int:patientPk>', views.analyse_list),
    path('new_analyse/<str:user>/<int:patientPk>', views.new_analyse),
    path('addImage/<int:analysePk>', views.addImage),
    path('make_analyse/<int:analysePk>', views.make_analyse),
    path('atest', views.atest),
    path('inference', views.inference),
]
