from django.contrib import admin
from django.urls import include, path
from main import  urls
from django.conf.urls.i18n import i18n_patterns
from django.conf.urls.static import static
from django.conf import settings



urlpatterns = i18n_patterns(
    path('admin/', admin.site.urls),
    path('', include('main.urls')),
prefix_default_language=False
)+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)